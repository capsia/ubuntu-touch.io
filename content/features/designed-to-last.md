---
title: 'Designed to last.'
priority: 0
displayInHome: true
text:
  -
    content: "Rather than being developed individually for each device, Ubuntu Touch runs the exact same operative system on all phones."
  -
    content: "This means that your phone will continue to receive software updates for years after traditional vendors would drop support."
---
