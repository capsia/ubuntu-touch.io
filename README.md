# Ubuntu Touch website

This is the development repository for the new Ubuntu Touch website (ubuntu-touch.io)
Site is live at https://ubuntu-touch.netlify.app

### Submit a new port

Learn how to [submit a port](https://gitlab.com/capsia/ubuntu-touch.io/-/wikis/Submit-a-new-port) to the website

### Start development

##### 1. Clone this repository:
`git clone https://gitlab.com/capsia/devices.ubuntu-touch.io.git`

##### 2. Install dependencies:
`npm install`

##### 3. Run on localhost:
`npm run-script develop`
