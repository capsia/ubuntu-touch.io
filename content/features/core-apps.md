---
title: 'Community Commit Together.'
priority: 0
displayInFeatures: true
image: '/img/features/coreapps.png'
text:
  -
    content: "Rather than being developed by one of the richest companies in the world, Ubuntu touch is fully developed by the community of users."
---
