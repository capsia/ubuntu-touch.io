---
question: 'Where do I report Ubuntu Touch bugs?'
displayInDevelopment: true
displayInDesign: true
displayInTesting: true
displayInMarketing: true
displayInCommunities: true
---

Whatever your goals are, we're here to help you be successful, no matter your measure of success. Are you trying to learn a new skill or get better at something you already know how to do? Are you looking for something to add to your CV for volunteering and community engagement? Maybe you just really love the mobile platform and want to see it thrive.
