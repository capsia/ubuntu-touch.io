---
title: 'Fully Transparent OS'
priority: 2
displayInHome: true
link:
  label: '>> Check our repo'
  target: '#'
image: '/img/homepage/media-1.jpg'
text:
  -
    content: "Developed as an open source project, Ubuntu Touch contains no hidden nuts and bolts."
  -
    content: "Your data are kept exactly where you want it: on your device, and nowhere else unless you explicitly ask for it."
---
