---
title: 'Community Comit Together.'
priority: 1
displayInHome: true
link:
  label: '>> Check community repo'
  target: '#'
image: '/img/homepage/commit.png'
text:
  -
    content: "Rather than being developed by one of the richest companies in the world, Ubuntu touch is fully developed by the community of users."
  -
    content: "As a consequence the system is developed with the sole purpose of giving the best and most secure user experience possible, while traditional systems balance these concern against profit."
---
