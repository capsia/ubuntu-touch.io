---
title: 'Community Commit Together.'
priority: 2
displayInFeatures: true
image: '/img/features/lomiri.png'
text:
  -
    content: "Rather than being developed by one of the richest companies in the world, Ubuntu touch is fully developed by the community of users."
---
